package com.example.moviesapp.adapters

import com.example.moviesapp.R
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.domain.entities.MoviesEntity
import com.squareup.picasso.Picasso


class RecyclerViewAdapter(var context: Activity, moviesArrayList: List<MoviesEntity>) :
    RecyclerView.Adapter<ViewHolder>() {
    var moviesArrayList: List<MoviesEntity> = moviesArrayList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView: View = LayoutInflater.from(context).inflate(R.layout.movies_recycle_viewer_row, parent, false)
        return RecyclerViewViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movies: MoviesEntity = moviesArrayList[position]
        val viewHolder =
            holder as RecyclerViewViewHolder
        viewHolder.txtView_title.setText(movies.title)
        viewHolder.txtView_description.setText(movies.overview)
        //with Picasso
        val picasso = Picasso.get()
        picasso.load("http://image.tmdb.org/t/p/w185" + movies.poster_path)
            .into(viewHolder.imgView_icon)
    }

    override fun getItemCount(): Int {
        return moviesArrayList.size
    }

    internal inner class RecyclerViewViewHolder(itemView: View) : ViewHolder(itemView) {
        var imgView_icon: ImageView
        var txtView_title: TextView
        var txtView_description: TextView

        init {
            imgView_icon = itemView.findViewById(R.id.imgView_icon)
            txtView_title = itemView.findViewById(R.id.txtView_title)
            txtView_description = itemView.findViewById(R.id.txtView_description)
        }
    }

}
