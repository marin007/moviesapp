package com.example.moviesapp.di

import com.example.domain.usecases.GetAllMoviesUseCase
import com.example.moviesapp.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    viewModel { MainViewModel(get()) }
}
val useCasesModule = module {
    single { GetAllMoviesUseCase(get()) }
}
