package com.example.moviesapp.ui.main

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.entities.MoviesEntity
import com.example.moviesapp.R
import com.example.moviesapp.adapters.RecyclerViewAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainFragment : Fragment() {

    var recyclerView: RecyclerView? = null
    var recyclerViewAdapter: RecyclerViewAdapter? = null

    companion object {
        fun newInstance() = MainFragment()
    }

    //private lateinit var viewModel: MainViewModel
    private val viewModel: MainViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.main_fragment, container, false)
        initUi(view)
        return view
    }

    private fun initUi(view: View){
        //view: View = inflater.inflate(R.layout.main_fragment, container, false)
        recyclerView = view.findViewById(R.id.rv_main);

        viewModel.getMovies()
        viewModel.moviesLiveData?.observe(viewLifecycleOwner, Observer {
            if(it?.size!! > 0){
                var moviesArrayList: List<MoviesEntity> = it as List<MoviesEntity>
                recyclerViewAdapter = RecyclerViewAdapter(context as Activity, moviesArrayList)
                recyclerView?.layoutManager = LinearLayoutManager(context)
                //recyclerView?.setLayoutManager(new LinearLayoutManager(context));
                recyclerView?.adapter = recyclerViewAdapter
            }
        })
    }

//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//        //viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
//        // TODO: Use the ViewModel
//    }

}