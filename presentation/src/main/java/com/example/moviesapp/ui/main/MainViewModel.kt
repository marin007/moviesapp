package com.example.moviesapp.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.domain.entities.MoviesEntity
import com.example.domain.usecases.GetAllMoviesUseCase
import com.google.gson.JsonElement
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver

class MainViewModel(private val getMoviesUseCase: GetAllMoviesUseCase) : ViewModel() {

    var moviesLiveData: MutableLiveData<List<MoviesEntity?>?>? = null
    var moviesArrayList: List<MoviesEntity>? = null

    fun getMovies() {
        getMoviesUseCase.execute(MoviesObserver())
//        //val thread = Thread(Runnable {
//            moviesLiveData = MutableLiveData()
//            moviesArrayList = getMoviesUseCase.getMovies()
////            moviesLiveData?.apply {
////                moviesLiveData?.value = moviesArrayList
////            }
//            moviesLiveData?.postValue(moviesArrayList)
//        //})
//        //thread.start()
    }
    private class MoviesObserver : io.reactivex.Observer<JsonElement> {
        override fun onComplete() {
            TODO("Not yet implemented")
        }

        override fun onSubscribe(d: Disposable) {
            TODO("Not yet implemented")
        }

        override fun onNext(t: JsonElement) {
            val response = t
            TODO("Not yet implemented")
        }

        override fun onError(e: Throwable) {
            val error = e
            TODO("Not yet implemented")
        }
    }

}