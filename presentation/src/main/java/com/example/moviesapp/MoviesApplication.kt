package com.example.moviesapp

import android.app.Application
import com.example.data.di.moviesModule
import com.example.data.di.networkModule
import com.example.moviesapp.di.useCasesModule
import com.example.moviesapp.di.viewModelsModule
import org.koin.core.context.startKoin

class MoviesApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(listOf(viewModelsModule, useCasesModule, networkModule, moviesModule))
        }
    }
}