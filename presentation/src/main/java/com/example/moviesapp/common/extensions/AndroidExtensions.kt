package com.example.moviesapp.common.extensions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

//inline fun ViewModel.launch(
//    coroutineContext: CoroutineContext = CoroutineContextProvider().main,
//    crossinline block: suspend CoroutineScope.() -> Unit): Job {
//    return viewModelScope.launch(coroutineContext) { block() }
//}