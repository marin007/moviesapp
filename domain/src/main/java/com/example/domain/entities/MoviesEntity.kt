package com.example.domain.entities

data class MoviesEntity (
    var popularity: Double,
    var vote_count: Int,
    var video: Boolean,
    var poster_path: String? = null,
    var title: String? = null,
    var id: Int,
    var vote_average: Double,
    var overview: String? = null
)