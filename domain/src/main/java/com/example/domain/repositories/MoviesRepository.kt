package com.example.domain.repositories

import com.example.domain.entities.MoviesEntity
import com.example.domain.model.Result
import com.google.gson.JsonElement
import io.reactivex.Observable


interface MoviesRepository {
    fun getMovies() : Observable<JsonElement>
}