package com.example.domain.usecases.base

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.schedulers.Schedulers

abstract class UseCase<T> {
    abstract fun buildUseCaseObservable(): Observable<T>
    fun execute(observer: Observer<T>?) {
        buildUseCaseObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(observer!!)
    }
}