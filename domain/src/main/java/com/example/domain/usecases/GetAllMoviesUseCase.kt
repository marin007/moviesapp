package com.example.domain.usecases

import com.example.domain.repositories.MoviesRepository
import com.example.domain.usecases.base.UseCase
import com.google.gson.JsonElement
import io.reactivex.Observable

class GetAllMoviesUseCase constructor(
    private val moviesRepository: MoviesRepository
) : UseCase<JsonElement>()  {
    override fun buildUseCaseObservable(): Observable<JsonElement> {
        return moviesRepository.getMovies()
    }
}