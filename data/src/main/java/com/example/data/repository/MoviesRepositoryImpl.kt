package com.example.data.repository

import com.example.data.api.RemoteMoviesApi
import com.example.domain.repositories.MoviesRepository
import com.google.gson.JsonElement
import io.reactivex.Observable


class MoviesRepositoryImpl(private val apiService: RemoteMoviesApi): MoviesRepository {
    override fun getMovies(): Observable<JsonElement> {
        return apiService.getMovies()
//        return fetchData(
//            dataProvider = {
//                apiService.getMovies().onSuccess { Success(it) }
//                apiService.getMovies().onFailure { Failure(it) }
//                Failure(HttpError(Throwable("Something went wrong, please try again.")))
//            }
////            apiDataProvider = {
////                apiService.getMovies().getData()
////            }
//        )
//        val call = apiService.getMovies()
//        val gson = GsonBuilder().setPrettyPrinting().create()
//        val response: Response<JsonElement> = call.execute()
//        if(response.isSuccessful){
//            val jsonObj = response.body() as JsonObject
//            val jsonResponse = jsonObj?.getAsJsonArray("results")
//            val movies = gson.fromJson(jsonResponse, Array<MoviesEntity>::class.java).toList()
//            return movies
//        } else {
//            return arrayListOf()
//        }

        // TODO make below async request return result callback
        //List<MoviesEntity>> tasks = call.execute().body();
//        call.enqueue { result ->
//            when(result) {
//                is Result.Success -> {
//                    //myData will have the type passed from the service method
//                    val myData = result.response.body()
//                }
//                is Result.Failure -> {
//                    Log.d("MyAndroidApp", result.error.toString())
//                }
//            }
//        }

//        call?.enqueue(object : Callback<JsonElement?> {
//            override fun onResponse(call: Call<JsonElement?>?, response: Response<JsonElement?>) {
//
//                val gson = GsonBuilder().setPrettyPrinting().create()
//                val jsonObj = response.body() as JsonObject
//                val jsonResponse = jsonObj.getAsJsonArray("results")
//                val movies = gson.fromJson(jsonResponse, Array<MoviesEntity>::class.java).toList()
//                moviesResponse = movies
//            }
//
//            override fun onFailure(call: Call<JsonElement?>?, t: Throwable?) {
//                //Handle failure
//            }
//        })
        //return arrayListOf()
    }

//    inline fun <reified T> Call<T>.enqueue(crossinline result: (Result<T>) -> Unit) {
//        enqueue(object: Callback<T> {
//            override fun onFailure(call: Call<T>, error: Throwable) {
//                result(Result.Failure(call, error))
//            }
//
//            override fun onResponse(call: Call<T>, response: Response<T>) {
//                result(Result.Success(call, response))
//            }
//        })
//    }
//    sealed class Result<T> {
//        data class Success<T>(val call: Call<T>, val response: Response<T>): Result<T>()
//        data class Failure<T>(val call: Call<T>, val error: Throwable): Result<T>()
//    }

}
