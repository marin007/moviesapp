package com.example.data.di

import com.example.data.api.RemoteMoviesApi
import com.example.data.repository.MoviesRepositoryImpl
import com.example.data.utils.ConnectivityImpl
import com.example.domain.repositories.MoviesRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val moviesModule = module {

    single { createMoviesRepository(get()) }
    //single { ConnectivityImpl(androidContext()) }
}
fun createMoviesRepository(apiService: RemoteMoviesApi): MoviesRepository {
    return MoviesRepositoryImpl(apiService)
}