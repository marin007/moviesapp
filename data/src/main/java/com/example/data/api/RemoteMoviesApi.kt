package com.example.data.api


import com.google.gson.JsonElement
import io.reactivex.Observable
import retrofit2.http.GET

interface RemoteMoviesApi {
    @GET("movie/upcoming?api_key=e59e4daae7eeb20e510a2a3af470347c&language=en-US&page=1") // TODO pass page and api key as params
    fun getMovies(): Observable<JsonElement>
}